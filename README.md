<h1>Django WebSite</h1>

<hr>

<h2>Stack</h2>
- *Python 3.10+*
- *Django 4.1.6+*
- *SQLite 3.22+*

<hr>

<h2>Installation</h2>
```bash
# Clone repository
$ git clone https://gitlab.com/nigmatul/django-website.git
```

```bash
# Update pip
$ python -m pip install --upgrade pip
```

```bash
# Create venv
$ python -m pip install -r requirements.txt
```

```bash
# Update database
$ python manage.py migrate
```

```bash
# Create superuser
$ python manage.py shell -c "from django.contrib.auth import get_user_model;
get_user_model().objects.create_superuser('Name', 'example@mail.domen', 'password')"
```

```bash
# Run web-site:
$ python manage.py runserver
```

<hr>

<h2>Credits</h2>

<hr>

<h2>License</h2>

<hr>